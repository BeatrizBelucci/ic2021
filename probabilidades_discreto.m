% probabilidades_discreto - Calculo das probabilidades para o classificador Bayesiano
% para atributos discretos.

% Copyright (c) 2021, Beatriz Belucci <beatrizbelucci04@gmail.com>

%    This program is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.

%    This program is distributed in the hope %%that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.

%    You should have received a copy of the GNU Genera%%l Public License
%    along with this program.  If not, see <https://www.gnu.org/licenses/>5.


function [P_A,P_B,P_a,P_b] = probabilidades_discreto(A,B)

    % function [P_A,P_B,P_a,P_b] = probabilidades_discreto(A,B) calcula as
    % probabilidade necessarias para a aplicacao do metodo do classificador
    % Bayesiano para atributos discretos. Tem como entrada as matriz A e B que
    % armazenam os objetos do conjunto de treinamento de duas classes distintas.
    % Como sa«¿da temos:    
   
    % P_a : probabilidade de ser da classe A.
    % P_b : probabilidade de ser da classe B.
    % P_A : frequencia relativa dos atributos no conjunto A.
    % P_B : frequencia relativa dos atributos no conjunto B.
    
    [m,n] = size(A);
    [j,k] = size(B);
  
    % m: total de objetos da classe A
    % j: total de objetos da classe B
    % n,k: total de atributos
  
    % Frequencia relativa de cada classe %

    P_a = m/(m + j); % probabilidade de ser da classe A
    P_b = j/(m + j); % probabilidade de ser da classe B

    % Frequencia relativa de cada atributo em cada um dos conjuntos A e B %
    
    for i=1:13
        P_A(i,1) = (length(find(A==i)))/m;
        P_B(i,1) = (length(find(B==i)))/j;  
    endfor
    
    % Com a funcao find() encontramos nas matrizes A e B quantos elementos
    % representam o atributo i e retorna um vetor com tais elementos. A funcao
    % lenght() retorna quantos elementos a funcao find() encontrou. Entao no
    % loop estamos contando quantos atributos de cada tipo existem nas matrizes
    % e dividindo pelo valor de atributos.
    % elementos representam o atributo i e retorna um vetor com tais
    % elementos. A funcao length() retorna quantos elementos a funcao
    % find() encontrou. Entao no loop estamos contando quantos atributos
    % de cada tipo existem nas matrizes e dividindo pelo valor de
    % atributos.
    
endfunction
