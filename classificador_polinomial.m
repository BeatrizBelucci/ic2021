%classificador_polinomial - Algoritmo para o classificador polinomial de grau 3
% com 4 variaveis

%Copyright (c) 2021, Beatriz Belucci <beatrizbelucci04@gmail.com>

%    This program is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.

%    This program is distributed in the hope %%that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.

%    You should have received a copy of the GNU Genera%%l Public License
%    along with this program.  If not, see <https://www.gnu.org/licenses/>5. 


function [classe] = classificador_polinomial(X,w)

% function classificador_polinomial(X,w) ��� um algoritmo que implementa o
% Classificador Polinomial para um polinomio de grau 3 com 4 variaveis. Tem como entrada a matriz X que armazena o conjunto de
% de objetos que serao classificados. Os elementos de X devem estar normalizados
% no intervalo [0,1]. O vetor w armazena os coeficientes que definem a superficie de
% decisao.
% Tem como saida o vetor classe, que armazena a classe dos objetos classificados. 

  
  for i = 1: size(X,1)
    % primeiro ��� construido o vetor q, que armazena todos as variaveis dos
    % monomios que compoem um polinomio de grau 3.
    x = X(i,:);
    M = x.*x';
    m1 = x(1).*[M(:,1)' M(2:end,2)' M(3:end,3)' M(4,4)'] ;
    m2 = x(2).*[M(2:end,2)' M(3:end,3)' M(4:end,4)'];
    m3 = x(3).*[M(3:end,3)' M(4:4)'];
    m4 = x(4).* M(4,4);  
    q = [1 x M(:,1)' M(2:end,2)' M(3:end,3)' M(4:end,4)' m1 m2 m3 m4];  
    soma = sum(q.*w); % soma: armazena a soma dos elementos q_i*w_i
    if soma < 0 
        classe(i) = 0;
      else
        classe(i) = 1;
      endif 
  endfor
  
endfunction