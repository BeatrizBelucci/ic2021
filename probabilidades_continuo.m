% probabilidades_continuo - Calculo das probabilidades para o classificador Bayesiano
% para atributos continuos.

%Copyright (c) 2021, Beatriz Belucci <beatrizbelucci04@gmail.com>

%    This program is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.

%    This program is distributed in the hope %%that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.

%    You should have received a copy of the GNU Genera%%l Public License
%    along with this program.  If not, see <https://www.gnu.org/licenses/>5.

function [n,P,M,V] = probabilidades_continuo(T)

    % function [n,P,M,V] = probabilidades_continuo(T) calcula as probabilidades necessarias
    % para a aplicacao do metodo do classificador Bayesiano para atributos
    % continuos. Como entrada recebe a matriz T que armazena o conjunto de treinamento.
    % Como saida temos:
    
    % n: numero total de classes.
    % P: vetor onde cada elemento e frequencia relativa de uma classes
    % M: matriz onde o elemento (i,j) e a media do atributo j na classe i
    % V: matriz onde o elemento (i,j) e a variancia ao quadrado do
    % atributo j na classe i

    n = max(T(:,end)); % n:  numero de classes
    
    % Separando os exemplos de treinamento por classes %
    
    [d,e] = size(T(:,1:end-1)); 

    % Separando os objetos de treino por classes %
    
    for ii=1:n                 
        I=find(T(:,end)== ii);
        C{ii}=T(I,1:end-1);
        a(1,ii) = rows(C{ii}); 
    endfor
  
    % Em cada iteracao, o loop encontra na matriz T qual vetor p de
    % indice u pertence a classe q e armazena esse vetor em uma lista
    % C{q} Ao final temos 3 listas C{q} uma para cada classe
    % a : armazena, por coluna, o numero de elementos em cada classe
  
    % Calculando a frequencia relativa de cada classe %
    
    P = a./d;                  

    % Calculando a media e a variancia de cada atributo por classe %
    
    for t = 1:n                 
        M(t,:)= mean(C{t});     
        V(t,:)= var(C{t}).^2;    
    endfor
    % M: matriz onde o elemento (i,j) e a media do atributo j na classe i
    % V: matriz onde o elemento (i,j) e a variancia ao quadrado do
    % atributo j na classe i 
endfunction 