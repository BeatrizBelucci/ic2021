% kNN_otimizado - Algoritmo para o metodo dos k-Vizinhos Mais Proximos Ponderado
% para um conjunto com 3 classes distintas.

% Copyright (c) 2021, Beatriz Belucci <beatrizbelucci04@gmail.com>

%    This program is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.

%    This program is distributed in the hope %%that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.

%    You should have received a copy of the GNU Genera%%l Public License
%    along with this program.  If not, see <https://www.gnu.org/licenses/>5. 

function [classe] = kNN_otimizado(T, x, k)
    % function [classe] = kNN_otimizado(T, x, k) implementa o metodo k-NN ponderado.
    % Tem como entrada uma matriz T que armazena o conjunto de treinamento, um vetor x de
    % atributos que descreve o objeto que queremos classificar e um escalar k  que representa
    % o numero de vizinhos que iremos analisar. Como saida temos a classe do objeto.

    ii = max(T(:,end)); 
  
    % ii armazena o numero de classes existentes no conjunto de treinamento
   
    [m,n] = size(T(:,1:end-1));
  
    % m armazena quantos objetos existem no conjunto de treinamento;
    % n armazena o numero de elementos do vetor de atributos.

    % Normalizacao do conjunto de treinamento e do objeto %
  
    maximo = max(T); % vetor com o maior valor de cada atributo
    minimo = min(T); % vetor com o menor valor de cada atributo
  
    for j=1: n
        T(:,j) =  (T(:,j) - minimo(j))/(maximo(j)- minimo(j));   
        x(:,j) = (x(:,j) - minimo(j))/(maximo(j)-minimo(j)); 
    endfor
  
    % Cada coluna da matriz T e do vetor x foram normalizadas
  
    % Calculando as metricas % 
  
    for j = 1: m
        d(j,1) = norm(x-T(j,1:n)); 
    endfor
  
    % a j-esima linha do vetor d armazena a metrica entre o
    % vetor x e o j-esimo vetor da matriz T
  
    [D,I] = sort(d);
    
    % D armazena os elementos de d por ordem crescente e I armazena a
    % posicao que os elementos de D estavam em d.
    
    D = D(1:k);      % escolhe as k menores distancias
    u = I(1:k);      % seleciona as k primeiras posicoes  
  
    % Calculando os pesos de cada atributo % 
   
    if D(1) == D(end)
        W = ones(m,1); 
    else    
        W = (D(end)- D)/(D(end)- D(1));    
    endif  
  
    % w armazenas os pesos dos atributos.
    % Se a menor distancia e a maior sao iguais, todos os pontos estao a
    % uma mesma distancia, logo o peso nao e necessario e o vetor w passa
    % a ser um vetor de entradas iguais a 1.
  
    C = T(u,end); % armazena as classes dos u-esimos elementos de T
  
    % Separando os objetos de treino por classes %

    for q=1:ii                
      p = find(T(u,end)== q); 
      C{q}=T(p,1:end-1);  
    endfor  

    % Em cada iteracao, o loop encontra na matriz T qual vetor p de
    % indice u pertence a classe q e armazena esse vetor em uma lista
    % C{q}. Ao final temos 3 listas C{q} uma para cada classe
    
    
    
    
    classe1 = 0;  classe2 = 0; classe3 = 0; % iniciando variaveis
  
    for h = 1:k
        if C(h)==1
          classe1 = classe1 + W(h);
        endif 
        if C(h)==2
          classe2 = classe2 + W(h);
        endif 
        if C(h)==3
          classe3 = classe3 + W(h);
        endif  
    endfor
  
    % Esse loop calcula a soma dos pesos de cada classe. Para cada valor
    % de k procura nas listas as posicoes dos vetores, pois, por
    % contrucao, a posicao do vetor na lista e a sua classe. Separa os
    % pesos para efetuar a soma, de acordo com as classes.
    
    % Classificando %
    
    Y = [classe1,classe2,classe3];
    [peso, classe] = max(Y); 
    
    % Y: vetor que armazena os pesos correspondentes a cada classe
    % peso: valor maximo dos pesos
    % classe: saida da funcao. Classe associada a variavel peso
    
endfunction 
