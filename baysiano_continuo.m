% bayesiano_continuo - Classificador Bayesiano para atributos coninuos

% Copyright (c) 2021, Beatriz Belucci <beatrizbelucci04@gmail.com>

%    This program is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.

%    This program is distributed in the hope %%that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.

%    You should have received a copy of the GNU Genera%%l Public License
%    along with this program.  If not, see <https://www.gnu.org/licenses/>5.

function [classe] = bayesiano_continuo(T,x)

    % function [classe] = bayesiano_continuo(T,x) e um algoritmo para o metodo do
    % classificador Bayesiano para atributos continuos. Como entrada recebe a matriz T que
    % armazena o conjunto de treinamento e o vetor x , que
    % representa o vetor de atributos do objeto que queremos classificar.
    % Como saida temos a classe do objeto.

    [n,P,M,V] = probabilidades_continuo(T)
   
    % Calculando a Funcao Gaussiana %
    for t = 1: n   
      k = 1./sqrt(2*pi*V(t,:)); 
      aux = ((x-M(t,:)).^2)./(2.*V(t,:));
      g = e.^(-aux);
      p_x(t,:) = k.*g; 
    endfor
    
    % k: coeficiente de normalizacao
    % p_x: Gaussiana. Matriz onde o elemento (i,j) e a FDP do atributo i
    % na classe j 
    
    z = prod(p_x');
    B = z.*P;
    [ii,jj] = max(B); 
    classe = jj;
    % z: p_ci(x)
    % B: numerador da formula de Bayes
    % ii: o valor maximo do numerador da formula de Bayes
    % jj: saida da funcao. Classe associada a ii
  
endfunction