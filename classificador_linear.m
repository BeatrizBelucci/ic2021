% classificador_linear - Algoritmo para o classificador linear

% Copyright (c) 2021, Beatriz Belucci <beatrizbelucci04@gmail.com>

%    This program is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.

%    This program is distributed in the hope %%that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.

%    You should have received a copy of the GNU Genera%%l Public License
%    along with this program.  If not, see <https://www.gnu.org/licenses/>5. 

function [classe] = classificador_linear(X,w)

% function classificador_linear(X,w) ��� um algoritmo que implementa o
% Classificador Linear. Tem como entrada a matriz X que armazena o conjunto de
% de objetos que serao classificados. Os elementos de X devem estar normalizados
% no intervalo [0,1]. O vetor w armazena os coeficientes que definem a superficie de
% decisao.
% Tem como saida o vetor classe, que armazena a classe dos objetos classificados.
  
  
  [m,n] = size(X); % m: numero de linhas da matriz X; n: numero de colunas da
                   % matriz X
  
  % Neste la���o ��� feita a classificacao a partir da analise do sinal resultante
  % da expressao da funcao afim da superficie de decisao
  
  X = [ones(rows(X),1) X]; #adiciona x0
  
  for i = 1: m
    soma = sum(X(i,:).*w); % soma: armazena a soma dos elementos w_i*x_i 
    if soma > 0
      classe(i) = 1 
    elseif soma < 0
      classe(i) = 0
    endif
  endfor
  
  
endfunction





