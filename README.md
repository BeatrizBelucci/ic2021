# PIBIC2020/2021

Esse projeto contém todos os algoritmos criados para implementar os métodos classificadores estudados no projeto de iniciação científica entre Setembro/2020 e Agosto/2021. Os métodos citados são: Classificador Bayesiano, Método k-NN, Classificador Linear, Classificador Polinomial. 
