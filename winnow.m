% winnow - Algoritmo WINNOW

% Copyright (c) 2021, Beatriz Belucci <beatrizbelucci04@gmail.com>

%    This program is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.

%    This program is distributed in the hope %%that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.

%    You should have received a copy of the GNU Genera%%l Public License
%    along with this program.  If not, see <https://www.gnu.org/licenses/>5. 


function [w] = winnow(T,C,R)
      
% function [w] = winnow(T,C,R) e uma funcao que executa o algoritmo
% WINNOW. Como entrada recebe a matriz T do conjunto de treinamento, o vetor que armazena 
% as classes dos elementos de T e a taxa R de aprendizagem.
% Como saida temos o vetor w que armazena os coeficientes que definem a
% superfie de decisao.  
        
  T = [ones(rows(T),1) T]; % adiciona x0
  
  [m,n] = size(T);         % m: numero de linhas da matriz X; n: numero de
                           % colunas das matriz X.
                           
  w = ones(1,n);           % inicializa o vetor w de coeficientes
  
  theta = n/2;             % atribui um valor inicial ao limiar 
  
  h = zeros(m,1);          % inicializa o vetor h das classes supostas pelo
                           % algoritmo
                           
 %Normalizacao do conjunto de treinamento 

  maximo = max(T); %retorna um vetor linha contendo os maiores valores de cada coluna de
  % T
  minimo = min(T); %retorna um vetor linha contendo os menores valores de cada coluna de
  % T


  for j=1: n 
    T(:,j) =  (T(:,j) - minimo(j))/(maximo(j)- minimo(j));     
  endfor                        
                                
                                                                                           
  % No la���o a seguir ��� feita a classificacao dos elementos do conjunto de treinamento.
  
  for i=1:m
    x = T(i,:);     
    soma = sum(w.*x); % soma: armazena a soma dos elementos w_i*x_i 
    if soma < theta 
      h(i) = 0;
    else
      h(i) = 1;
    endif
   if C(i) != h(i) % compara a classe real com a classe suposta pelo algoritmo
     aux = find(x != 0); % determina quais atributos sao diferentes de zero
     w(aux) = w(aux).*(R.^(C(i)-h(i))); % atualiza os pesos w_i
   endif
   p = norm(C-h,1);
    if p == 0  % verifica se a classe suposta e igual a classe real 
      break
    endif
  endfor  
endfunction
  

      
      
      
