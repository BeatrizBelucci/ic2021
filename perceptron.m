% perceptron - Algoritmo Perceptron Learning

% Copyright (c) 2021, Beatriz Belucci <beatrizbelucci04@gmail.com>

%    This program is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.

%    This program is distributed in the hope %%that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.

%    You should have received a copy of the GNU Genera%%l Public License
%    along with this program.  If not, see <https://www.gnu.org/licenses/>5. 


function [w] = perceptron(T,C,R)

% function [w] = perceptron(X,C,R) ��� uma funcao que executa o algoritmo
% Perceptron Learning. Como entrada recebe a matriz T do conjunto de treinamento,
% o vetor C que armazena as classes dos elmentos de X e a taxa R de
% aprendizagem.
% Como saida temos o vetor w que armazena os coeficientes que definem a
% superfie de decisao.  
  
   
  T = [ones(rows(T),1) T]; #adiciona x0
  
 [m,n] = size(T);  % m: numero de linhas da matriz X; n: numero de
                   % colunas das matriz X.                  
  
  
  
  w = rand(1,n);    % inicializa o vetor w de coeficientes
  
  h = zeros(m,1);   % inicializa o vetor h das classes supostas pelo
                    % algoritmo
                    
  p = norm(C-h,inf); % verifica se a classe suposta e igual a classe real                       
         
  
  while p != 0    
    for i=1:m
      x = T(i,:);     
      soma = sum(w.*x); % soma: armazena a soma dos elementos w_i*x_i 
      if soma < 0 
        h(i) = 0;
      else
        h(i) =1;
      endif 
      w = w+[R*(C(i)-h(i)).*x]; % atualiza os pesos w_i
    endfor    
    p = norm(C-h,inf);   
  endwhile   
endfunction
  

      
      
      
