% bayesiano_discreto - Classificador Bayesiano para atributos discretos

% Copyright (c) 2021, Beatriz Belucci <beatrizbelucci04@gmail.com>

%    This program is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.

%    This program is distributed in the hope %%that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.

%    You should have received a copy of the GNU Genera%%l Public License
%    along with this program.  If not, see <https://www.gnu.org/licenses/>5.


function [Classificacao] = bayesiano_discreto(A,B,T)

    % function [Classificacao] = bayesiano_discreto(A,B,T) e um algoritmo para o classificador
    % Bayesiano para atributos discretos. Tem como entrada as matrizes A e B, que armazenam
    % os vetores de atributos dos objetos da classe A e B, respectivamente. 
    % Recebe tambem, a matriz T que armazena o conjunto de teste.

    [P_A,P_B,P_a,P_b] = probabilidades_discreto(A,B)
    
  
    [s,t] = size(T) 
  
    % s: numero de objetos que serao classificados 
    % t: numero de atributos      


    for i = 1:s
        G(i,1) = prod(P_A(T(i,:)));
        G(i,2) = prod(P_B(T(i,:)));
    endfor
        
    % O loop calcula o produto das frequencias relativas dos atributos da
    % matriz T. A matriz G armazena na primeira coluna esse produto referente a
    % classe A e na segunda coluna referente a classe B
    
        
    %  Calculando o numerador da formula de Bayes %
    
    G(:,1) = G(:,1)*P_a;
    G(:,2) = G(:,2)*P_b;
  
    % Calculado a classe e a porcentagem de confianca da classificacao %
    
    r = G(:,1) + G(:,2);
    
    % r: soma das probabilidade ser da classe A e da classe B
    
    Porcentagem = (G./r)*100; 
    
    % Porcentagem: porcentagem de confianca da classificacao
    
    [P,C] = max(Porcentagem(k,:);
    
    % P: maior porcentagem entre as duas classes
    % C: classes associadas a P
     
    Classificacao = [C P]; 
    
    % Classificacao: saida da funcao. Retorna C e P
  
endfunction